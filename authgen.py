#! /usr/bin/env python3

#By Alex Mori & Calvin Hu
import hashlib
counter = 1
flag = True
seed = "810780FF00FF07012"

#Will continue to generate OTP codes until user selects no
while flag:
    val = raw_input('generate authentication code? Yes/No ')
    if val == "Yes":
        #flag = True
        counter += 1
        #updates the new seed value
        seed = str(hashlib.sha256(seed).hexdigest())
        #truncates the hash to form OTP but least significant 6 digital
        hash1 = seed[58:]
        #prints OTP Token
        print('Authentication Token:')
        print(hash1)
    else:
        #Exits out of loop
        flag = False
